/* ScrollReveal Customize */

window.sr = ScrollReveal();
sr.reveal('#left-features', {duration: 1000});
sr.reveal('#right-features', {duration: 1000});

sr.reveal('.d-green');
sr.reveal('.d-orange', {delay: 200});
sr.reveal('.d-blue', {delay: 400});

sr.reveal('#f1');
sr.reveal('#f2', {delay: 200});
sr.reveal('#f3', {delay: 400});
sr.reveal('#f4', {delay: 600});
