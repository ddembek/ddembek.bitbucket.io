$(function() {

	function hoverEffect(el, el_content) {

		$(el).hover(function() {
			//$(this).find(el_content).show();
			$(this).find(el_content).css('visibility', 'visible');
			$(this).find(el_content).css('opacity', '1');
		});

		$(el).mouseleave(function() {
			//$(this).find(el_content).hide();
			$(this).find(el_content).css('visibility', 'hidden');
			$(this).find(el_content).css('opacity', '0');
		});
	}

	hoverEffect(".work", ".work-content");
	hoverEffect(".member", ".member-content");

	function navScroll() {
		$(window).scroll(function() {

			var scrollPosition = $(window).scrollTop();
			
			if(scrollPosition != 0) {

				$('.navbar').removeClass('bg-green-o');
				$('.navbar').addClass('bg-green');

			} else {

				$('.navbar').removeClass('bg-green');
				$('.navbar').addClass('bg-green-o');
			}

		});
	}

	navScroll();

	function factsAnimate() {	

		$(window).scroll(function() {

			var test = $('#facts').hasClass('animated');
			var isVisible = $('#facts').visible();

			if(!test && isVisible) {
				
				$('#facts').addClass('animated');

				var numAnim = new CountUp("hours", 0, 3200);
				if (!numAnim.error) {
				    numAnim.start();
				} else {
				    console.error(numAnim.error);
				}

				var numAnim = new CountUp("clients", 0, 120);
				if (!numAnim.error) {
				    numAnim.start();
				} else {
				    console.error(numAnim.error);
				}

				var numAnim = new CountUp("projects", 0, 360);
				if (!numAnim.error) {
				    numAnim.start();
				} else {
				    console.error(numAnim.error);
				}

				var numAnim = new CountUp("awards", 0, 42);
				if (!numAnim.error) {
				    numAnim.start();
				} else {
				    console.error(numAnim.error);
				}
			}

		});
		
	}

	factsAnimate();

	function sectionScroll() {

		$('.navbar-nav .nav-link').click(function() {

			var link = $(this).attr('href');
			
			$('html, body').animate({
				scrollTop: $(link).offset().top
			}, 500);

		});
	}

	sectionScroll();
	

});