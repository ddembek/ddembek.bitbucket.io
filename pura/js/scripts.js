$(function() {

	function mobileMenu() {

		$('.open').click(function() {
			$('.navbar-menu').css('display', 'flex');
		});

		$('.close').click(function() {
			$('.navbar-menu').css('display', 'none');
		});

		$(window).resize(function() {

			var window_width = $(window).width();
			var navbar_status = $('.navbar-menu').css('display');

			if(window_width >= 992 && navbar_status == 'none') {
				$('.navbar-menu').css('display', 'flex');
			}
		});
	}

	mobileMenu();

});