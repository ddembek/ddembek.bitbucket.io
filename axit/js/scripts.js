$(function() {

	$(window).scroll(function() {
		var scroll_position = $(window).scrollTop();

		if(scroll_position > 150) {
			$('.top-scroll').show(600).css('display', 'flex');
		} else {
			$('.top-scroll').hide(600);
		}
	});

	$('.top-scroll').click(function() {
		$('body, html').animate({scrollTop: 0}, 500);
	});

	$('.navbar-nav a').click(function() {

		var section_id = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(section_id).offset().top
		}, 500);

	});

});